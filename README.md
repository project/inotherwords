# In Other Words

## Table of Contents
* Introduction
* Vision
* Values
* Contributing
** Roadmap
** Reporting Bugs and Requesting Features
* Requirements
* Installation
* Configuration
* Maintainers
* Supporting Organizations
* License

## Introduction
In Other Words allows site builders to configure lists of items to display with natural language. 

For example, a multivalue set of flavors could be configured to output this: 

_Our flavors include chocolate, vanilla, strawberry, and peppermint chocolate chip.

## Vision

The In Other Words module makes it easy for websites to display lists naturally, making your content more readable and accessible.

## Values

The In Other Words module is: 
* **powerful** for site builders
* **sustainable** for site owners
* **out of the way** of themers and developers


## Contributing

We appreciate contributions of all kind for the project.

If you haven't already, familiarize yourself with [Drupal's Code of Conduct](https://www.drupal.org/dcoc). 

We design improvements and prioritize issues based on our vision and values.

### Roadmap

Visit our [Roadmap](https://www.drupal.org/project/inotherwords/issues/3349751) to see which issues are a priority and in need of assistance.

### Reporting Bugs and Requesting Features

* To submit bug reports and feature suggestions, visit https://www.drupal.org/project/issues/inotherwords

## Requirements

This module relies on the following PHP libraries: 

* agaric/oxford-comma ^1.2
* kwn/number-to-words ^1.8

If you install In Other Words via composer, these libraries will be brought in automatically.

## Installation
Install In Other Words as you would normally
install a contributed Drupal module.
Read the [Installing Modules documentation page](https://www.drupal.org/node/1897420) for more information.

## Configuration

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Structure > Content types [Content type to edit]
3. Navigate to Content Types > Manage display.
4. Select the Format select list for the desired field and choose your desired In Other Words field formatter.
5. Configure the field to display lists to your preference.
6. Save the Manage display form.

For more information read the [In Other Words Documentation](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-modules/in-other-words)

## Maintainers

* [Benjamin Melançon (mlncn)](https://www.drupal.org/u/mlncn)
* [Clayton Dewey (cedewey)](https://www.drupal.org/u/cedewey)
* [David Valdez (gnuget)](https://www.drupal.org/u/gnuget)
* [Keegan Rankin (MegaKeegMan)](https://www.drupal.org/u/megakeegman)

## Supporting Organizations

This module exists and is maintained in large part thanks to:

[Agaric Technology Collective](https://agaric.coop)<br>
<img src="https://drupal.org/files/styles/grid-2/public/agaric-logo-stacked.png" alt="Agaric Logo."/>

[DevCollaborative](https://devcollaborative.com)<br>
<img src="https://devcollaborative.com/themes/custom/dc2020lightship/logo.svg" alt="DevCollaborative Logo."/>

[Drutopia](https://drutopia.org)

## License
GPL-2.0+
