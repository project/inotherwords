<?php

namespace Drupal\inotherwords\Plugin\Field\FieldFormatter;

/**
 * Shared common traits for In Other Words formatters.
 *
 * @internal
 */
trait InotherwordsFormatterHelperTrait {

  /**
   * Common elements in formatter defaultSettings functions.
   */
  public static function commonDefaultSettings() {
    return [
      'series_join' => ', ',
      'series_final_join' => ' and ',
      'text_before' => '',
      'text_after' => '',
      //      'all_items_text' => 'All @field_name',
      'oxford_comma' => TRUE,
    ];
  }

  public function commonSettingsFormElements() {
    $elements = [];
    $elements['series_join'] = [
      '#title' => t('Join symbol or separator between list items'),
      '#description' => t('To allow the possibility of creating a sequence without spaces, typically include the space after your series joining symbol, for example ", " or "; ".'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('series_join'),
    ];
    $elements['series_final_join'] = [
      '#title' => t('Final join word'),
      '#description' => t('The word for the final join in the series, typically " and " or " or ".  Spaces required unless your final join symbol should not have spacing.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('series_final_join'),
    ];
    $elements['oxford_comma'] = [
      '#title' => t('Connect final items with a series comma where applicable'),
      '#description' => ('Items connected with the final join symbol / separator above (by default ", ") will also have this symbol used before the final join word if there are at least three items in the series: In other words, it will use the oxford comma: "State Fair, South Pacific, and The King and I".'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('oxford_comma'),
    ];
    $elements['text_before'] = [
      '#title' => t('Text before'),
      '#description' => ('Before all other output, start with this, for example: "Ages ".  Enter a blank space after your text <em>unless</em> it should not have any space at all between the before text and the sequences.  To have different text for a single item, add it <em>after</em> the plural text for multiple items, separated by a pipe, for example "Categories: |Category: ".  This text can be used in place of the inline label.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('text_before'),
    ];
    $elements['text_after'] = [
      '#title' => t('Text after'),
      '#description' => ('After all other output end with this, for example: " years old".  If there should be a space between the last item and this text, include the space.  This allows skipping the space if wanted, such as to end with just a period: "."'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('text_after'),
    ];

    $elements['help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Want more options?  Open issues in the <a href=":issues_url" target="_blank">issue queue</a>.', [':issues_url' => 'https://www.drupal.org/project/issues/inotherwords']),
      '#weight' => 999,
    ];

    return $elements;
  }

  public static function processPlural($text, $single) {
    $texts = explode('|', $text);
    if (count($texts) > 1) {
      $text = $single ? $texts[1] : $texts[0];
    }
    return $text;
  }
}
