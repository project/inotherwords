<?php

namespace Drupal\inotherwords\Plugin\Field\FieldFormatter;

use function Agaric\OxfordComma\oxford_comma_list;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime_range\DateTimeRangeTrait;
use Drupal\inotherwords\Plugin\Field\FieldFormatter\InotherwordsFormatterHelperTrait;
use Drupal\smart_date_recur\Plugin\Field\FieldFormatter\SmartDateRecurrenceFormatter;
use NumberToWords\NumberToWords;

/**
 * Plugin implementation of InOtherWords formatter for daterange fields.
 *
 * Attempts to make a sensible summary of a series of date ranges, removing
 * duplicate data and taking into account what the real-world date currently is.
 *
 * @FieldFormatter(
 *   id = "inotherwords_date_ranges",
 *   label = @Translation("In other words: Date ranges"),
 *   field_types = {
 *     "smartdate"
 *   }
 * )
 */
class InotherwordsDateRangeFormatter extends SmartDateRecurrenceFormatter {

  use DateTimeRangeTrait;
  use InotherwordsFormatterHelperTrait;


  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'time' => FALSE,
        'past' => 'deprioritized',
        'past_by_day' => TRUE,
        'upcoming' => 'shown',
        'upcoming_limit' => 0,
        'upcoming_overflow' => 'hidden',
        'recurring_statement' => t('This is a recurring opportunity.'), // Really hoping Drupal has or figures out a set play for translatable configuration for field formatters.
        'text_before_upcoming' => t('Upcoming occurrences include: '),
        'text_after_upcoming' => '.',
        'format_type' => 'inotherwords_date',
        'no_year_format_type' => 'inotherwords_no_year',
        'time_only_format_type' => 'inotherwords_time_only',
        'shorten_time' => TRUE,
        'series_join' => '; ',
      ] + self::commonDefaultSettings() + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // We need to load parent elements first because we make changes to them,
    // specifically the date_format title and description.
    $elements = parent::settingsForm($form, $form_state);
    $elements['past'] = [
      '#type' => 'select',
      '#title' => $this->t('Past dates'),
      '#options' => ['shown' => 'Show', 'hidden' => 'Hide', 'deprioritized' => 'Deprioritize'],
      '#default_value' => $this->getSetting('past'),
    ];
    $elements['past_by_day'] = [
      '#title' => t('Show events that happen today independently of the time'),
      '#description' => ('If unchecked, the next date shown will be based on the current time. Otherwise, midnight of today.'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('past_by_day'),
    ];
    $elements['recurring_statement'] = [
      '#title' => t('Recurring statement'),
      '#description' => t('Optional text to display if there are multiple dates.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('recurring_statement'),
    ];
    $elements['upcoming'] = [
      '#type' => 'select',
      '#title' => $this->t('Upcoming dates'),
      '#options' => ['shown' => 'Show', 'hidden' => 'Hide', 'deprioritized' => 'Deprioritize'],
      '#description' => $this->t('The very next date is always shown.  This option determines if and how any of multiple upcoming dates beyond the first one are displayed.'),
      '#default_value' => $this->getSetting('upcoming'),
    ];
    $elements['upcoming_limit'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 100,
      '#title' => $this->t('Upcoming dates limit'),
      '#description' => $this->t('Limit initial upcoming shown per above to this number (blank or 0 for unlimited)'),
      '#default_value' => $this->getSetting('upcoming_limit'),
    ];
    $elements['upcoming_overflow'] = [
      '#type' => 'select',
      '#title' => $this->t('Overflow dates beyond upcoming limit'),
      '#options' => ['shown' => 'Show', 'hidden' => 'Hide', 'deprioritized' => 'Deprioritize'],
      '#description' => $this->t('How to display any upcoming dates beyond the initial limit, if set.'),
      '#default_value' => $this->getSetting('upcoming_overflow'),
    ];
    $elements['text_before_upcoming'] = [
      '#title' => t('Text before upcoming dates'),
      '#description' => t('Text to introduce additional dates following the first, if any, and if displayed.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('text_before_upcoming'),
    ];
    $elements['text_after_upcoming'] = [
      '#title' => t('Text after upcoming dates'),
      '#description' => t('Text or punctuation to follow the list of additional dates, if any, and if displayed.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('text_after_upcoming'),
    ];

    // Set up formats wich are the options for several different settings.
    $format_types = $this->dateFormatStorage->loadMultiple();
    $options = [];
    $options[''] = $this->t('-- Not used --');
    foreach ($format_types as $type => $type_info) {
      $format = $this->dateFormatter->format(time(), $type);
      $options[$type] = $type_info->label() . ' (' . $format . ')';
    }

    // Same year date format (leave off the year)
    $elements['no_year_format_type'] = [
      '#type' => 'select',
      '#title' => t('No year date format'),
      '#description' => t("Choose a format for displaying the date when it is in the current year.  Again, date only.  If a time should be displayed set 'Time format' below."),
      '#options' => $options,
      '#default_value' => $this->getSetting('no_year_format_type'),
    ];
    // Main date format (leave off the time) - we override the title and
    // description to be clear that the time should be left off, but rather
    // than making this the special case for same day dates, we just add in
    // the separated time in different places depending on whether it's a
    // same day date situation or not.
    $elements['format_type']['#title'] = t('Date format (no time)');
    $elements['format_type']['#description'] = t("Choose a format for displaying the date only.  If a time should displayed set 'Time format' below.");
    // Time format (leave off the date) used for same-day events.
    $options[''] = $this->t('-- No time displayed --');
    $elements['time_only_format_type'] = [
      '#type' => 'select',
      '#title' => t('Time format'),
      '#description' => t("If your date range has times, choose a time-only date format for displaying the time (this needed separate so when a datetime range is on one day, so the date is shown once and only the time is printed as a range)."),
      '#options' => $options,
      '#default_value' => $this->getSetting('time_only_format_type'),
    ];

    $elements['shorten_time'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Shorten on-the-hour time output'),
      '#description' => $this->t('For instance, a time of 3:30pm stays the same but if a time is 3:00pm display as 3pm.'),
      '#default_value' => $this->getSetting('shorten_time'),
    ];

    return $elements + $this->commonSettingsFormElements();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Display date ranges with past @past and any upcoming beyond the next one @upcoming.',
      array(
        '@past' => $this->getSetting('past'),
        '@upcoming' => $this->getSetting('upcoming'),
      )
    );

    return $summary;
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $original_settings = $this->settings;
    $elements = parent::viewElements($items, $langcode);
    // Restore our settings after SmartDateRecurrenceFormatter rudely overwrites.
    $this->settings = array_merge($original_settings, $this->settings);

    // For now, if *any* element is using an RRULE, return what Smart Date
    // gives us, with no attempt at our own modifications.
    foreach ($elements as $element) {
      if (isset($element['#rule_text'])) {
        return $elements;
      }
    }
    $parts = [];

    // This will be what we return instead of elements.
    $inotherwords = [];

    $default_timezone = \Drupal::config('system.date')->get('timezone.default');
    // This might be harmful; it may work better to keep it in UTC?
    date_default_timezone_set($default_timezone);
    if ($this->getSetting('past_by_day')) {
      $now = strtotime('today');
    }
    else {
      $now = strtotime('now');
    }

    $past_count = 0;
    $upcoming_count = 0;
    $proximate = NULL;
    $single = TRUE;
    $shorten_time = $this->getSetting('shorten_time');
    $separator_element = ['#plain_text' => $this->getSetting('separator')];

    foreach ($items as $delta => $item) {
      if (!empty($item->value) && !empty($item->end_value)) {
        $start_date = $item->value;
        $end_date = $item->end_value;
        $date_format_normal = $this->getSetting('date_format') ?: 'D, F j Y';

        // TODO integrate with / switch to Smart Date formatting rather than pretty much doing it independently.
        // See https://www.drupal.org/project/smart_date/issues/3266104 for probably last feature we need to switch

        // If it's the current year and we have a date format set with no year.
        if ($this->getSetting('no_year_format_type') && date('Y', $now) === date('Y', $start_date)) {
          $start_date_formatted = $this->formatDate($start_date, $this->getSetting('no_year_format_type'));
        }
        else {
          $start_date_formatted = date($date_format_normal, $start_date);
        }
        if ($this->getSetting('no_year_format_type') && date('Y', $now) === date('Y', $end_date)) {
          $end_date_formatted = $this->formatDate($end_date, $this->getSetting('no_year_format_type'));
        }
        else {
          $end_date_formatted = date($date_format_normal, $end_date);
        }
        $start_date_formatted .= ' ';
        $end_date_formatted .= ' ';

        // If start and end are *identical* there will be no separator so we
        // only add the date and time and nothing else.
        if ($start_date === $end_date && $this->getSetting('time_only_format_type')) {
          // If it's not the same day as today, include the date.
          if (date('Y-m-d', $now) !== date('Y-m-d', $start_date)) {
            $parts[$delta]['date']['#plain_text'] = $start_date_formatted;
          }
          $parts[$delta]['time']['#plain_text'] = $this->formatDate($start_date, $this->getSetting('time_only_format_type'), $shorten_time);
        }
        // If start date date matches end date date, condense to one and.
        // @TODO restore this
        // Note that the ISO datetime attribute adheres to these original
        // start and end dates, built with buildDateWithIsoAttribute(), and we
        // don't duplicate that attribute when including the time.
        if (date('Y-m-d', $start_date) === date('Y-m-d', $end_date)) {
          $parts[$delta]['date']['#plain_text'] = $start_date_formatted;
          if ($this->getSetting('time_only_format_type')) {
            $parts[$delta]['start_time']['#plain_text'] = $this->formatDate($start_date, $this->getSetting('time_only_format_type'), $shorten_time);
            $parts[$delta]['separator'] = $separator_element;
            $parts[$delta]['end_time']['#plain_text'] = $this->formatDate($end_date, $this->getSetting('time_only_format_type'), $shorten_time);
          }
        }
        else {
          $parts[$delta]['start_date']['#plain_text'] = $start_date_formatted;
          if ($this->getSetting('time_only_format_type')) {
            $parts[$delta]['start_time']['#plain_text'] = $this->formatDate($start_date, $this->getSetting('time_only_format_type'), $shorten_time);
          }
          $parts[$delta]['separator'] = $separator_element;
          $parts[$delta]['end_date']['#plain_text'] = $end_date_formatted;
          if ($this->getSetting('time_only_format_type')) {
            $parts[$delta]['end_time']['#plain_text'] = $this->formatDate($end_date, $this->getSetting('time_only_format_type'), $shorten_time);
          }
        }
        if (!isset($parts[$delta]['start_date']['#plain_text']) && !isset($parts[$delta]['date']['#plain_text'])) {
          \Drupal::logger('inotherwords')
            ->notice("Unexpected result that a daterange did not have either a start date nor combined date.", []);
        }

        // If it's in the past.
        if ($now > $end_date) {
          $parts[$delta]['_inotherwords_grouping'] = 'past';
          ++$past_count;
        }
        else {
          $parts[$delta]['_inotherwords_grouping'] = 'upcoming';

          // Keep track of which event is coming up next (aka current event).
          // We will use winning delta to swap 'upcoming' group for 'proximate'.
          if (is_null($proximate) || $start_date < $items[$proximate]->value) {
            $proximate = $delta;
          }
          ++$upcoming_count;
        }

      }
      else {
        \Drupal::logger('inotherwords')->notice("Unexpected result that a daterange did not have both a start and end date.", []);
      }

    } // End $items foreach

    // If there's no upcoming date, print a message about past.
    if (is_null($proximate)) {
      $inotherwords['past'][]['#markup'] = '<h5 class="title is-5">' . $this->t('This opportunity has past.') . '</h5>';
    }
    // If there is an upcoming date, override upcoming group with proximate.
    else {
      unset($parts[$proximate]['_inotherwords_grouping']);
      --$upcoming_count;
      // Put proximate (AKA current or very next) item in a group by itself.
      $inotherwords[100][] = [
        '#theme' => 'inotherwords_dateranges_proximate__' . $this->viewMode,
        '#proximate' => $parts[$proximate],
      ];
      unset($parts[$proximate]);
    }

    // For some inexplicable reason first keys in a renderable array must be integers?
    // Note these integers are not honored by order, but we use them below to
    // reorder the results: 'past' => 9000, 'upcoming' => 200.
    $series = [];
    foreach ($parts as $part) {
      if (isset($part['_inotherwords_grouping'])) {
        $group = $part['_inotherwords_grouping'];
        unset($part['_inotherwords_grouping']);
        $series[$group][] = $part;
      }
    }

    $text_before = $this->getSetting('text_before');
    $text_after = $this->getSetting('text_after');
    // Use the series comma for each grouped series.  Set this up now for use
    // by calls of oxford_comma_list for past and/or upcoming series below.
    $oxford_comma_settings = [
      'join' => ['#markup' => $this->getSetting('series_join')],
      'final_join' => ['#markup' => $this->getSetting('series_final_join')],
    ];

    if ($past_count && $this->getSetting('past') !== 'hidden') {
      $past_series = oxford_comma_list($series['past'], $oxford_comma_settings);
      $word_number = $this->numberToWords($past_count);

      $inotherwords[9000]['past'] = [
        '#theme' => 'inotherwords_dateranges_past',
        '#count' => $past_count,
        '#word_count' => $word_number,
        '#html_id' => Html::getUniqueId('inotherwords-toggle-past'),
        '#items' => ['#theme' => 'inotherwords_series__dateranges_past__' . $this->viewMode, '#items' => $past_series],
        '#deprioritized_class' => $this->getSetting('past') === 'deprioritized' ? 'inotherwords--start-hidden' : 'inotherwords--start-visible',
        '#attached' => [
          'library' => [
            'inotherwords/toggle',
          ],
        ]
      ];
    }

    if ($upcoming_count && $this->getSetting('upcoming') !== 'hidden') {
      $upcoming_limit = (int) $this->getSetting('upcoming_limit');
      $recurring_statement = $this->getSetting('recurring_statement');
      $deprioritized_class = $this->getSetting('upcoming') === 'deprioritized' ? 'inotherwords--start-hidden' : 'inotherwords--start-visible';
      $deprioritized_class_initial = '';
      $upcoming = $series['upcoming'];
      // OK.  So this is confusing in code, and it'll probably be confusing in
      // the user interface, but it's logical, i swear.  What we're doing is if
      // there is a limit set, and so overflow additional upcoming events, we
      // use the regular 'show/hidden' setting to apply to just the initial.
      if ($upcoming_limit) {
        // This code had two array_splice's in a row before, one to get the
        // initial and the second to get the rest (upcoming limit as the offset
        // with no length to get all the rest.  This was unnecessary and indeed
        // actively harmful, because array_splice takes the array it is given
        // by reference and *removes* what it returns from it.  Doing
        // array_splice twice in a row without realizing it caused a bug where
        // the next $upcoming_limit events were not shown.  In short:
        // array_splice removes spliced elements from $upcoming for us.
        $upcoming_initial = array_splice($upcoming, 0, $upcoming_limit);
        // If we're showing just the initial few, then set the rest of the count to zero to hide that whole section.
        // If we choose to give an option to see the number of additional upcoming beyond the initial limit, but not show the dates, this would need to change.
        $upcoming_count = $this->getSetting('upcoming_overflow') !== 'hidden' ? count($upcoming) : FALSE;
        $upcoming_series_initial = oxford_comma_list($upcoming_initial, $oxford_comma_settings);
        $deprioritized_class_initial = $deprioritized_class;
        $deprioritized_class = $this->getSetting('upcoming_overflow') === 'deprioritized' ? 'inotherwords--start-hidden' : 'inotherwords--start-visible';
        $hideable_initial = $this->getSetting('upcoming') === 'deprioritized';
      }
      $upcoming_series = oxford_comma_list($upcoming, $oxford_comma_settings);
      $word_number = $this->numberToWords($upcoming_count);
      $inotherwords[200]['items'] = [
        '#theme' => 'inotherwords_dateranges_upcoming',
        '#recurring_statement' => $recurring_statement,
        '#count_initial' => $upcoming_limit,
        '#word_count_initial' => ($upcoming_limit) ? $this->numberToWords($upcoming_limit) : '',
        '#html_id_initial' => Html::getUniqueId('inotherwords-toggle-upcoming-initial'),
        '#deprioritized_class_initial' => $deprioritized_class_initial,
        '#hideable_initial' => $hideable_initial,
        '#items_initial' => ($upcoming_limit) ? ['#theme' => 'inotherwords_series__dateranges_upcoming__initial__' . $this->viewMode, '#items' => $upcoming_series_initial] : '',
        '#count' => $upcoming_count,
        '#word_count' => $word_number,
        '#html_id' => Html::getUniqueId('inotherwords-toggle-upcoming'),
        '#deprioritized_class' => $deprioritized_class,
        '#text_before' => $this->getSetting('text_before_upcoming'),
        '#items' => ['#theme' => 'inotherwords_series__dateranges_upcoming__' . $this->viewMode, '#items' => $upcoming_series],
        '#text_after' => $this->getSetting('text_after_upcoming'),
      ];
      $single = FALSE;
    }

    // php reorder array based on index: https://www.php.net/manual/en/function.ksort.php
    ksort($inotherwords);

    array_unshift($inotherwords, ['#markup' => self::processPlural($text_before, $single)]);

    $inotherwords[] = ['#markup' => self::processPlural($text_after, $single)];

    return $inotherwords;
  }


  /**
   * Override format date to allow passing in format type and short time option.
   *
   * @param int $date
   *
   * @param string $format_type
   *
   * @param bool $shorten_time
   *
   * @return string
   */
  protected function formatDate($date, $format_type = '', $shorten_time = FALSE) {
    if (!$format_type) {
      $format_type = $this->getSetting('format_type');
    }
    $timezone = $this->getSetting('timezone_override') ?: NULL;
    date_default_timezone_set($timezone);
    $output = $this->dateFormatter->format($date, $format_type, '', $timezone != '' ? $timezone : NULL);
    if ($shorten_time) {
      $output = str_replace(':00', '', $output);
    }
    return $output;
  }

  /**
   * Helper function to convert an integer into a written word.
   *
   * @param integer $number
   *
   * @return string
   */
  public function numberToWords($number) {
    $numberToWords = new NumberToWords();
    $numberTransformer = $numberToWords->getNumberTransformer('en'); // @TODO multilingual
    return $numberTransformer->toWords($number);
  }

}
