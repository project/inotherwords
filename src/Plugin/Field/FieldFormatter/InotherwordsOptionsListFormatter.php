<?php

namespace Drupal\inotherwords\Plugin\Field\FieldFormatter;

use function Agaric\OxfordComma\oxford_comma_list;
use Agaric\OxfordComma;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inotherwords\Plugin\Field\FieldFormatter\InotherwordsFormatterHelperTrait;

/**
 * Plugin implementation of the 'inotherwords list' formatter.
 *
 * It applies to entity reference items.
 *
 * The entity reference items are output as labels; it's not practical to do
 * this for full rendered entities, although the labels themselves are output as
 * renderable arrays, so it wouldn't be that different...
 *
 * @FieldFormatter(
 *   id = "inotherwords_options_list",
 *   label = @Translation("In other words: List"),
 *   description = @Translation("Provide format settings including series comma such that given Apple Banana Cantaloupe as selected options content shows Apple, Banana, and Cantaloupe."),
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   }
 * )
 */
class InotherwordsOptionsListFormatter extends OptionsDefaultFormatter {
  use InotherwordsFormatterHelperTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return self::commonDefaultSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
  // Consider bringing this back if we want to go through the trouble of getting
  // all the possible items and iterating through them.
//    $elements['all_items_text'] = [
//      '#title' => t('All items text'),
//      '#description' => t('Words to <em>replace</em> output with if every item was selected.  Make blank to not do this.'),
//      '#type' => 'textfield',
//      '#default_value' => $this->getSetting('all_items_text'),
//    ];

    return $this->commonSettingsFormElements() + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('List items separated by "@series_join" with "@series_final_join" at the end, prefaced by "@text_before" and followed by "@text_after".',
      [
        '@series_join' => $this->getSetting('series_join'),
        '@series_final_join' => $this->getSetting('series_final_join'),
        '@text_before' => $this->getSetting('text_before'),
        '@text_after' => $this->getSetting('text_after'),
      ]
    );
    // $all_items_text = $this->getSetting('all_items_text');
    // $summary[] = $all_items_text ? t('If all items are selected the text output will be "@all_items_text"',
    //  ['@all_items_text' => $all_items_text]) : '';
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    // This will be what we return instead of elements.
    $inotherwords = [];

    // Not sure if we'd ever be given zero elements, but if so, give 'em back.
    if (!$elements) {
      return $elements;
    }

    $single = count($elements) === 1;

    $series_final_join = $this->getSetting('series_final_join');
    $series_join = $this->getSetting('series_join');
    $text_before = $this->getSetting('text_before');
    $text_after = $this->getSetting('text_after');

    $oxford_comma_settings = [
      'join' => ['#markup' => $series_join],
      'final_join' => ['#markup' => $series_final_join ],
      'oxford' => $this->getSetting('oxford_comma'),
    ];

    $inotherwords = oxford_comma_list($elements, $oxford_comma_settings);

    return [
      '#theme' => 'inotherwords_series_wrapped',
      '#text_before' => self::processPlural($text_before, $single),
      '#items' => $inotherwords,
      '#text_after' => self::processPlural($text_after, $single),
    ];
  }

}
