<?php

namespace Drupal\inotherwords\Plugin\Field\FieldFormatter;

use function Agaric\OxfordComma\oxford_comma_list;
use Agaric\OxfordComma;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'inotherwords sequential terms' formatter.
 *
 * It applies to entity reference taxonomy terms.
 *
 * We're only set up to handle taxonomy terms right now, because we get the
 * full order from their vocabulary, but the same thing could be done for text
 * lists.  It would take a bit of a lift but ideally the comparison of one list
 * to another, after the items have been normalized, can be moved to a separate
 * PHP library.
 *
 * In any case, it's cleaner to make this work for text lists in a separate
 * formatter.
 *
 * @FieldFormatter(
 *   id = "inotherwords_sequential_terms",
 *   label = @Translation("In other words: Sequential terms"),
 *   description = @Translation("Summarize sequential taxonomy terms; given
 *   <em>Monday, Tuesday, Wednesday</em> return <em>Monday through Wednesday</em>."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class InotherwordsSequentialTermsFormatter extends EntityReferenceLabelFormatter {
  use InotherwordsFormatterHelperTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'connecting_word' => 'through',
      'minimum_sequence' => 4,
      'all_items_text' => 'All @field_name',
      'link' => FALSE,
      'treat_as_one' => [],
      'treat_as_one_label' => '',
    ] + self::commonDefaultSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['connecting_word'] = [
      '#title' => t('Connecting word'),
      '#description' => t('Word or symbol (such as " to " or "–") to replace the removed items in a sequence.  Note that <em>any</em> spaces you would like must be included here.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('connecting_word'),
    ];
    $elements['minimum_sequence'] = [
      '#title' => t('Minimum number of items in a sequence before removing middle items'),
      '#description' => t('Minimum number of items in a sequence before removing middle items'),
      '#type' => 'number',
      '#min' => 3,
      '#default_value' => $this->getSetting('minimum_sequence'),
    ];
    $elements['all_items_text'] = [
      '#title' => t('All items text'),
      '#description' => t('Words to <em>replace</em> output with if every item was selected.  Make blank to not do this.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('all_items_text'),
    ];
    $elements['link'] = [
      '#title' => t('Link label to the referenced entity'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('link'),
    ];

    $terms = $this->getAllTerms();
    $options = [];
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }
    $elements['treat_as_one'] = [
      '#title' => t('Treat as one'),
      '#description' => t('Treat selected items as one if two or more of them are included in a series.'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $options,
      '#default_value' => $this->getSetting('treat_as_one'),
    ];
    $elements['treat_as_one_label'] = [
      '#title' => t('Treat as one label'),
      '#description' => t('Label to substitute for items that are treated as one.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('treat_as_one_label'),
    ];

    return $elements + $this->commonSettingsFormElements() + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Series of at least @minimum_sequence items drop intermediary items in favor of "@connecting_word" as a connector',
      [
        '@minimum_sequence' => $this->getSetting('minimum_sequence'),
        '@connecting_word' => $this->getSetting('connecting_word'),
      ]
    );
    $all_items_text = $this->getSetting('all_items_text');
    $summary[] = $all_items_text ? t('If all items are selected the text output will be "@all_items_text"',
      ['@all_items_text' => $all_items_text]) : '';
    $summary[] = $this->getSetting('link') ? t('Try to link to referenced entities') : t('No link');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    // This will be what we return instead of elements.
    $inotherwords = [];

    // Not sure if we'd ever be given zero elements, but if so, give 'em back.
    if (!$elements) {
      return $elements;
    }

    $single = count($elements) === 1;

    $output_as_link = $this->getSetting('link');
    $connecting_word = $this->getSetting('connecting_word');
    $text_before = $this->getSetting('text_before');
    $text_after = $this->getSetting('text_after');
    $minimum_sequence = $this->getSetting('minimum_sequence');
    $all_items_text = $this->getSetting('all_items_text');
    $treat_as_one = $this->getSetting('treat_as_one');
    $treat_as_one_label = $this->getSetting('treat_as_one_label');

    $sequences = $this->getSequencedElements($elements, $items, $treat_as_one, $treat_as_one_label, $output_as_link, $langcode);

    if (!isset($sequences['all_items'])) {
      // We didn't actually make sequences, what was returned is just elements.
      return $sequences; // Equal to $elements.
    }
    $all_items = $sequences['all_items'];
    // Get sequences back to being a nested, but all-numeric-key, array.
    unset($sequences['all_items']);

    $handler_settings = $this->fieldDefinition->getSetting('handler_settings');
    $vocabulary_id = current($handler_settings['target_bundles']);

    if ($all_items && $all_items_text) {
      $field_name = $this->fieldDefinition->getLabel();
      $field_machine_name = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('label');
      $inotherwords[0] = [
        '#markup' => new FormattableMarkup($all_items_text, [
          '@field_name' => $field_name,
          '@field_machine_name' => $field_machine_name,
          '@vocabulary_machine_name' => $vocabulary_id,
        ]),
      ];
      // @TODO if $output_as_link is TRUE make a hover-over with all the links.
      return $inotherwords;
    }

    foreach ($sequences as $sequence_id => $sequence) {
      if (count($sequence) >= $minimum_sequence) {
        $last_element = end($sequence);
        $first_element = reset($sequence);
        $inotherwords[$sequence_id][] = $first_element;
        $inotherwords[$sequence_id][] = ['#markup' => " $connecting_word "];
        $inotherwords[$sequence_id][] = $last_element;
        if ($this->getSetting('oxford_comma')) {
          // Mark it as a summarized sequence; we will remove this later.
          $inotherwords[$sequence_id]['summarized'] = TRUE;
        }
      }
      else {
        // Single items not in a consecutive sequence still need to be output!
        $inotherwords[$sequence_id] = $sequence;
      }
    }

    $oxford_comma_settings = [
      'join' => ['#markup' => $this->getSetting('series_join')],
      'final_join' => ['#markup' => $this->getSetting('series_final_join')],
    ];


    // Make every series, and the groupings of the series, grammatical.
    if ($this->getSetting('oxford_comma')) {
      foreach ($inotherwords as $sequence_id => $sequence) {
        // Elements of a summarized sequence are items like "Monday", "through",
        // "Thursday".  We don't want to turn that into an Oxford comma series!
        if (!isset($sequence['summarized'])) {
          $inotherwords[$sequence_id] = ['#theme' => 'inotherwords_series', '#items' => oxford_comma_list($sequence, $oxford_comma_settings)];
        }
        else {
          // Don't leave our summarized metadata lying around.
          unset($inotherwords[$sequence_id]['summarized']);
        }
      }
    }

    // In case we have multiple sequences, join *the sequences themselves*
    // together with natural language, serial comma, too.
    $inotherwords = oxford_comma_list($inotherwords, $oxford_comma_settings);

    return [
      '#theme' => 'inotherwords_series_wrapped',
      '#text_before' => self::processPlural($text_before, $single),
      '#items' => $inotherwords,
      '#text_after' => self::processPlural($text_after, $single),
    ];
  }

  /**
   * Returns elements sorted into sequences of consecutive items.
   *
   * @param $elements
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The item list.
   * @param array $treat_as_one
   *   A list of term IDs to treat as one item for sequencing purposes.
   * @param string $treat_as_one_label
   *   The name to use for the consolidated 'term'.
   * @param bool $output_as_link
   *   Whether to try to output elements as links (at least unsummarized ones).
   * @param string $langcode
   *   The language code of the referenced entities to display.
   *
   * @return array $sequences
   *   The elements to display, nested in consecutive sequences.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see ::viewElements()
   */
  public function getSequencedElements($elements, FieldItemListInterface $items, $treat_as_one, $treat_as_one_label, $output_as_link, $langcode) {
    $ordered_terms = $this->getAllTerms();

    $label_key = ($output_as_link) ? '#title' : '#plain_text';
    $label = $elements[0][$label_key];
    $treated_as_one = [];
    $treated_as_one_count = 0;

    // Get the location of the first item we're given to display in the full
    // source it was drawn from.  As of PHP7, we can use the `use` keyword to
    // pass a value into the anonymous function that yields a closure.
    $first = array_filter($ordered_terms, function($term) use ($label) {
      return $term->name === $label;
    });
    $next_key = key($first);

    // If our first item isn't in the source, there's been some mistake somehow
    // so leave without doing anything and know if something blows up it's not
    // our fault.
    if ($next_key === NULL) {
      return $elements;
    }

    // We count on the given items and the full list of items to be in the same
    // order.  We'd have to re-order the given terms if that were not the case.

    $sequences = [];
    $sequence_id = 0;
    $key = 0;
    // By default figure every element from the full potential list is present.
    // And yes we're mixing associative array keys with numeric keys, but we'll
    // clean it up later.
    $sequences['all_items'] = TRUE;
    while (TRUE) {
      // Advance to our first or next matching item in the ordered items.
      if (key($ordered_terms) === $next_key) {
        // Add any items in sync with ordered items.
        $current_element = current($elements);
        if (current($ordered_terms)->name !== $current_element[$label_key]) {
          \Drupal::logger('inotherwords')->notice("Unexpected result that all terms term %name did not equal current element %title.", [
            '%name' => current($ordered_terms)->name,
            '%title' => $current_element[$label_key],
          ]);
        } else {
          // All is as expected, add the element to the current sequence.
          $sequences[$sequence_id][$key] = $current_element;
          // Mark terms to be treated as one for potential consolidation.
          if ($treat_as_one && in_array(current($ordered_terms)->tid, $treat_as_one)) {
            $treated_as_one_count++;
            $treated_as_one[$sequence_id][] = $key;
          }
          $key++;
        }
        $next_element = next($elements);
        // When we're out of elements to sequence, we can leave the loop.
        if ($next_element === FALSE) {
          // We must 'break' and not return because the treated as one
          // treatment occurs just after the loop in this method.
          break;
        }
        // Use our next element's name to get matching ordered items' key.
        $label = $next_element[$label_key];
        $next = array_filter($ordered_terms, function($term) use ($label) {
          return $term->name === $label;
        });
        $next_key = key($next);
      }
      else {
        // If we got here, we had to skip one of the ordered items.
        $sequence_id++;
        $sequences['all_items'] = FALSE;
      }
      // Remove any possibility of an infinite loop (but should never happen).
      if (key($ordered_terms) === NULL) {
        break;
      };
      next($ordered_terms);
    }

    if ($treated_as_one_count >= 2) {
      $treated_as_one_not_transformed = TRUE;
      foreach ($treated_as_one as $sequence_id => $keys) {
        foreach ($keys as $key) {
          // If this is an element we are going to treat as one, and it's the
          // first one, set the label and record the precedent, otherwise we
          // remove it.
          if ($treated_as_one_not_transformed) {
            $sequences[$sequence_id][$key][$label_key] = $treat_as_one_label;
            $treated_as_one_not_transformed = FALSE;
          }
          else {
            unset($sequences[$sequence_id][$key]);
          }
        }
      }
    }

    return $sequences;
  }

  /**
   * Get available terms in the order they were put in on the taxonomy page.
   */
  public function getAllTerms() {
    $handler_settings = $this->fieldDefinition->getSetting('handler_settings');
    $vocabulary_id = current($handler_settings['target_bundles']);
    // @TODO pass in manager: https://drupal.stackexchange.com/a/224248/4195
    $TermStorage = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term');
    return $TermStorage->loadTree($vocabulary_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for taxonomy terms.
    return $field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'taxonomy_term';
  }

}
