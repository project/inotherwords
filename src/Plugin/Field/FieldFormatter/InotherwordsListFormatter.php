<?php

namespace Drupal\inotherwords\Plugin\Field\FieldFormatter;

use function Agaric\OxfordComma\oxford_comma_list;
use Agaric\OxfordComma;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inotherwords\Plugin\Field\FieldFormatter\InotherwordsFormatterHelperTrait;

/**
 * Plugin implementation of the 'inotherwords list' formatter.
 *
 * It applies to entity reference items.
 *
 * The entity reference items are output as labels; it's not practical to do
 * this for full rendered entities, although the labels themselves are output as
 * renderable arrays, so it wouldn't be that different...
 *
 * @FieldFormatter(
 *   id = "inotherwords_list",
 *   label = @Translation("In other words: List"),
 *   description = @Translation("Provide natural series, including the series
 *   (Oxford) comma; given Apple Banana Cantaloupe as entity labels, terms, or
 *   list items, return <em>Apple, Banana, and Cantaloupe</em>."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class InotherwordsListFormatter extends EntityReferenceLabelFormatter {
  use InotherwordsFormatterHelperTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return self::commonDefaultSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
  // Consider bringing this back if we want to go through the trouble of getting
  // all the possible items and iterating through them.
//    $elements['all_items_text'] = [
//      '#title' => t('All items text'),
//      '#description' => t('Words to <em>replace</em> output with if every item was selected.  Make blank to not do this.'),
//      '#type' => 'textfield',
//      '#default_value' => $this->getSetting('all_items_text'),
//    ];
    $elements['link'] = [
      '#title' => t('Link label to the referenced entity'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('link'),
    ];

    return $elements + $this->commonSettingsFormElements() + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('List items separated by "@series_join" with "@series_final_join" at the end, prefaced by "@text_before" and followed by "@text_after".',
      [
        '@series_join' => $this->getSetting('series_join'),
        '@series_final_join' => $this->getSetting('series_final_join'),
        '@text_before' => $this->getSetting('text_before'),
        '@text_after' => $this->getSetting('text_after'),
      ]
    );
    // $all_items_text = $this->getSetting('all_items_text');
    // $summary[] = $all_items_text ? t('If all items are selected the text output will be "@all_items_text"',
    //  ['@all_items_text' => $all_items_text]) : '';
    $summary[] = $this->getSetting('link') ? t('Link to referenced entities') : t('No link');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    // This will be what we return instead of elements.
    $inotherwords = [];

    // Not sure if we'd ever be given zero elements, but if so, give 'em back.
    if (!$elements) {
      return $elements;
    }

    $single = count($elements) === 1;

    $series_final_join = $this->getSetting('series_final_join');
    $series_join = $this->getSetting('series_join');
    $text_before = $this->getSetting('text_before');
    $text_after = $this->getSetting('text_after');

    $oxford_comma_settings = [
      'join' => ['#markup' => $series_join],
      'final_join' => ['#markup' => $series_final_join ],
      'oxford' => $this->getSetting('oxford_comma'),
    ];

    $inotherwords = oxford_comma_list($elements, $oxford_comma_settings);

    // Construct further context for theme suggestions.
    $theme_suggest = '';
    // Use the null coalescer to be certain we do not throw errors.
    $theme_suggest_view_mode = $this->viewMode ?? '';
    $theme_suggest_field_name = $items->getName() ?? '';
    // Add the preceeding underscores to valid extensions to the suggestion.
    $theme_suggest .= $theme_suggest_view_mode ? '__' . $theme_suggest_view_mode : '';
    $theme_suggest .= $theme_suggest_field_name ? '__' . $theme_suggest_field_name : '';

    // Needless nesting needed for FormatterBase::view() to add field wrappers.
    $elements = [];
    $elements[0] = [
      '#theme' => 'inotherwords_series_wrapped__list' . $theme_suggest,
      '#text_before' => self::processPlural($text_before, $single),
      '#items' => $inotherwords,
      '#text_after' => self::processPlural($text_after, $single),
    ];
    return $elements;
  }

}
