<?php

namespace Drupal\Tests\inotherwords\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Tests\examples\Functional\ExamplesBrowserTestBase;

/**
 * Class InotherwordsBrowserTestBase.
 *
 * @group inotherwords
 */
abstract class InotherwordsBrowserTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The content type name.
   *
   * @var string
   */
  protected $contentTypeName;

  /**
   * The administrator account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $administratorAccount;

  /**
   * The author account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $authorAccount;

  /**
   * The field name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['block', 'node', 'field_ui', 'text', 'options', 'inotherwords', 'taxonomy'];

  /**
   * {@inheritdoc}
   *
   * Once installed, a content type with the desired field is created.
   */
  protected function setUp(): void {
    // Install Drupal.
    parent::setUp();

    // Add the system menu blocks to appropriate regions.
    // This includes the local tasks that have the "Add field" link.
    $this->setupExamplesMenus();

    // Create and login a user that creates the content type.
    $permissions = [
      'administer content types',
      'administer node fields',
      'administer node form display',
      'administer node display',
      'administer taxonomy',
    ];
    $this->administratorAccount = $this->drupalCreateUser($permissions);
    parent::drupalLogin($this->administratorAccount);

    // Prepare a new content type where the field will be added.
    $this->contentTypeName = strtolower($this->randomMachineName(10));
    $this->drupalGet('admin/structure/types/add');
    $edit = [
      'name' => $this->contentTypeName,
      'type' => $this->contentTypeName,
    ];
    $this->submitForm($edit, 'Save and manage fields');
    $this->assertSession()->pageTextContains((string) new FormattableMarkup('The content type @name has been added.', ['@name' => $this->contentTypeName]));

    // Reset the permission cache.
    $create_permission = 'create ' . $this->contentTypeName . ' content';
    $this->checkPermissions([$create_permission]);

    // Now that we have a new content type, create a user that has privileges
    // on the content type.
    $this->authorAccount = $this->drupalCreateUser([$create_permission]);
  }

  /**
   * Set up menus and tasks in their regions.
   *
   * Since menus and tasks are now blocks, we're required to explicitly set them
   * to regions. This method standardizes the way we do that for Examples.
   *
   * Note that subclasses must explicitly declare that the block module is a
   * dependency.
   */
  protected function setupExamplesMenus() {
    $this->drupalPlaceBlock('system_menu_block:tools', ['region' => 'primary_menu']);
    $this->drupalPlaceBlock('local_tasks_block', ['region' => 'secondary_menu']);
    $this->drupalPlaceBlock('local_actions_block', ['region' => 'content']);
    $this->drupalPlaceBlock('page_title_block', ['region' => 'content']);
  }

  /**
   * Create a field on the content type created during setUp().
   *
   * @param string $type
   *   The storage field type to create.
   * @param string $widget_type
   *   The widget to use when editing this field.
   * @param int|string $cardinality
   *   Cardinality of the field. Use -1 to signify 'unlimited'.
   * @param string $fieldFormatter
   *   The formatter to use when editing this field.
   *
   * @return string
   *   Name of the field, like field_something
   */
  protected function createField($type = 'list_string', $widget_type = 'options_select', $cardinality = '-1', $fieldFormatter = 'inotherwords_options_list') {
    if ($type == 'field_ui:entity_reference:taxonomy_term') {
      $this->createVocab();
    }

    $assert = $this->assertSession();

    $this->drupalGet('admin/structure/types/manage/' . $this->contentTypeName . '/fields');

    // Go to the 'Add field' page.
    $this->clickLink('Add field');

    // Make a name for this field.
    $field_name = strtolower($this->randomMachineName(10));

    // Fill out the field form.
    $edit = [
      'new_storage_type' => $type,
      'field_name' => $field_name,
      'label' => $field_name,
    ];
    $this->submitForm($edit, 'Save and continue');

    // Fill out the $cardinality form as if we're not using an unlimited number
    // of values.
    $edit = [
      'cardinality' => 'number',
      'cardinality_number' => (string) $cardinality,
    ];
    // If we have -1 for $cardinality, we should change the form's drop-down
    // from 'Number' to 'Unlimited'.
    if (-1 == $cardinality) {
      $edit = [
        'cardinality' => '-1',
        'cardinality_number' => '1',
      ];
    }

    // We'll just hard-code a set of text values if setting up an option list.
    if ($type == 'list_string') {
      $edit['settings[allowed_values]'] = 'a|Alfa
b|Bravo
c|Charlie
d|Delta
e|Echo
f|Foxtrot';
    }

    // And now we save the cardinality settings.
    $this->submitForm($edit, 'Save field settings');
    $assert->pageTextContains((string) new FormattableMarkup('Updated field @name field settings.', ['@name' => $field_name]));

    if ($type == 'field_ui:entity_reference:taxonomy_term') {
      $edit_settings = [];
      $edit_settings['settings[handler_settings][target_bundles][example_taxonomy]'] = TRUE;
      $this->submitForm($edit_settings, 'Save settings');
      $assert->pageTextContains((string) new FormattableMarkup('Saved @name configuration.', ['@name' => $field_name]));
    }

    // Set the widget type for the newly created field.
    $this->drupalGet('admin/structure/types/manage/' . $this->contentTypeName . '/form-display');
    $edit_widget = [
      'fields[field_' . $field_name . '][type]' => $widget_type,
    ];
    $this->submitForm($edit_widget, 'Save');

    // Set the field formatter for the newly created field.
    $this->drupalGet('admin/structure/types/manage/' . $this->contentTypeName . '/display');
    $edit_formatter = [
      'fields[field_' . $field_name . '][type]' => $fieldFormatter,
    ];
    $this->submitForm($edit_formatter, 'Save');

    // $this->drupalGet('admin/structure/types/manage/' . $this->contentTypeName . '/display');
    // $edit_formatter_settings = [
    //  'fields[field_' . $field_name . '][settings_edit_form][settings][text_before]' => 'Behold: ',
    // ];
    // $this->submitForm($edit_formatter_settings, 'Save');

    return $field_name;
  }

  function createVocab() {
    $this->drupalGet('admin/structure/taxonomy/add');
    $edit = [
      'name' => 'Example taxonomy',
      'vid' => 'example_taxonomy',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains((string) new FormattableMarkup('Created new vocabulary @name. ', ['@name' => 'Example taxonomy']));
  }
}
