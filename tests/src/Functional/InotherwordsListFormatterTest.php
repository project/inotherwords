<?php

namespace Drupal\Tests\inotherwords\Functional;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Test basic functionality of the widgets.
 *
 * Create a content type with a example_field_rgb field, configure it with the
 * field_example_text-widget, create a node and check for correct values.
 *
 * @group inotherwords
 *
 * @ingroup inotherwords
 */
class InotherwordsListFormatterTest extends InotherwordsBrowserTestBase {

  /**
   * Test basic functionality of the text list field formatter.
   *
   * - Creates a content type.
   * - Adds a multivalued list_string to it.
   * - Creates a node of the new type.
   * - Populates the multivalued field with three items.
   * - Tests the result for a series comma list.
   */
  public function testOptionListOxfordField() {
    $assert = $this->assertSession();

    // Add a single field as administrator user.
    $this->drupalLogin($this->administratorAccount);
    $this->fieldName = $this->createField('list_string', 'options_select', '-1', 'inotherwords_options_list');

    // Now that we have a content type with the desired field, switch to the
    // author user to create content with it.
    $this->drupalLogin($this->authorAccount);
    $this->drupalGet('node/add/' . $this->contentTypeName);

    // Add a node.
    $title = $this->randomMachineName(20);
    $edit = [
      'title[0][value]' => $title,
      'field_' . $this->fieldName . '[]' => ['a', 'c', 'f'],
    ];

    // Create the content.
    $this->submitForm($edit, 'Save');
    $assert->pageTextContains("$this->contentTypeName $title has been created");

    // Verify the value is shown when viewing this node.
    $assert->pageTextContains('Alfa, Charlie, and Foxtrot');
  }

 /**
   * Test basic functionality of the entity reference list field formatter.
   *
   * - Creates a content type.
   * - Adds a multivalued term reference to it.
   * - Creates a node of the new type.
   * - Populates the multivalued term reference with three items.
   * - Tests the result for a series comma list.
   */
  public function testTaxonomyListOxfordField() {
    $assert = $this->assertSession();

    // Add a single field as administrator user.
    $this->drupalLogin($this->administratorAccount);
    $this->fieldName = $this->createField('field_ui:entity_reference:taxonomy_term', 'options_select', '-1', 'inotherwords_list');

    // Add vocabulary to Example taxonomy for use in content creation
    $this->drupalGet('/admin/structure/taxonomy/manage/example_taxonomy/add');
    $edit = ['name[0][value]' => 'Alpha'];
    $this->submitForm($edit, 'Save');
    $assert->pageTextContains("Created new term Alpha.");
    $edit = ['name[0][value]' => 'Beta'];
    $this->submitForm($edit, 'Save');
    $assert->pageTextContains("Created new term Beta.");
    $edit = ['name[0][value]' => 'Gamma'];
    $this->submitForm($edit, 'Save');
    $assert->pageTextContains("Created new term Gamma.");


    // Now that we have a content type with the desired field, switch to the
    // author user to create content with it.
    $this->drupalLogin($this->authorAccount);
    $this->drupalGet('node/add/' . $this->contentTypeName);

    // Add a node.
    $title = $this->randomMachineName(20);
    $edit = [
      'title[0][value]' => $title,
      'field_' . $this->fieldName . '[]' => ['1', '2', '3'],
    ];

    // Create the content.
    $this->submitForm($edit, 'Save');
    $assert->pageTextContains("$this->contentTypeName $title has been created");

    // Verify the value is shown when viewing this node.
    $assert->pageTextContains('Alpha, Beta, and Gamma');
  }

}
